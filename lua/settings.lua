------------------------------------------------------------------------------
-- Базовые настройки
-------------------------------------------------------------------------------

vim.opt.number = true 	                -- Включаем нумерацию строк
vim.opt.so = 999                          -- Курсор всегда в центре экрана
vim.opt.undofile = true                 -- Возможность отката назад
vim.opt.splitright = true               -- Vertical split вправо
vim.opt.splitbelow = true               -- Horizontal split вниз
vim.opt.swapfile = false                -- Отключить swapfile

-------------------------------------------------------------------------------
-- Цвета и внешний вид
-------------------------------------------------------------------------------

vim.opt.termguicolors = true             -- Использовать цвета терминала
vim.cmd'colorscheme gruvbox'      -- Цветовая схема
require'nvim-treesitter.configs'.setup { -- Подсветка синтаксиса treesitter
  -- Список парсеров языков
  ensure_installed = { "python", "c", "cpp", "lua", "java", "html", "php"},

  -- Парсеры языков из ensure install будут установлены автоматически
  sync_install = false,

  -- Те языки которые следует игнорировать для автоматической установки парсера
  ignore_install = {},

  highlight = {
    -- Включить подсветку синтаксиса
    enable = true,

    -- Те языки которые не нужно подсвечивать когда подсветка включена
    disable = {},
  },
  autotag = {
    -- Автозакрытие html/xml тэгов с помощью treesitter
    enable = true,
    -- Включить
  },
}
require'colorizer'.setup()              -- Отображать HEX как цвета
require'transparent'.setup()            -- Прозрачность фона
require'nvim-autopairs'.setup {}
-------------------------------------------------------------------------------
-- Пробелы, отступы, табы
-------------------------------------------------------------------------------

vim.cmd([[
filetype indent plugin on
syntax on
]])
vim.opt.expandtab = true                -- Улучшенные настройки табов
vim.opt.shiftwidth = 4                  -- Таб как 4 пробела
vim.opt.tabstop = 4                     -- Ширина таба = 4 пробела
vim.opt.smartindent = true              -- "Умный" автоотступ

-- 2 пробeла отступа в некоторых форматах
vim.cmd [[
  autocmd FileType xml,html,xhtml,css,scss,java,javascript,lua,yaml,htmljinja setlocal shiftwidth=2 tabstop=2
]]

-------------------------------------------------------------------------------
-- Прочее
-------------------------------------------------------------------------------

-- Подсвечивает на доли секунды скопированную часть текста
vim.api.nvim_exec([[   
augroup YankHighlight
autocmd!
autocmd TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=700}
augroup end
]], false)

vim.cmd'set clipboard+=unnamedplus'     -- Копирование в общий системный буфер
vim.cmd'let g:netrw_banner = 0'
vim.cmd'let g:netrw_liststyle = 3'
vim.cmd'let g:netrw_browse_split = 3'

-------------------------------------------------------------------------------
-- Language Server Protocol и Autocompletition
-------------------------------------------------------------------------------

--require'java'.setup()
require'lspconfig'.jdtls.setup({})
require'lspconfig'.lua_ls.setup{} -- Запуск lua_language_server для Lua
require'lspconfig'.pyright.setup{} -- Запуск Pyright для Python
require'lspconfig'.clangd.setup{} -- Запуск clangd для C/C++
require'toggle_lsp_diagnostics'.init({ start_on = false }) -- Включён ли LSP по-умолчанию
vim.o.completeopt = 'menuone,noselect' -- Запуск автодополнения

vim.api.nvim_exec([[   
autocmd BufNewFile,BufRead *.html set filetype=html
]], false) -- Форсированный тип файла html при расширении *.html (надо переписать на lua)
