require "paq" {
    -- Цветовая схема gruvbox
    "ellisonleao/gruvbox.nvim";

    -- Автозакрытие скобок
    "windwp/nvim-autopairs";

    -- Автозакрытие HTML/XML тэгов
    "windwp/nvim-ts-autotag";

    -- Цвета HEX подсвечиваются на фоне
    "norcalli/nvim-colorizer.lua";

    -- Обрамление кавычками и скобками одной коммандой
    "tpope/vim-surround";

    -- Включение и выключение прозрачности фона
    "xiyaowong/nvim-transparent";

    -- LSP конфигурация для neovim
    "neovim/nvim-lspconfig";

    -- Улучшенная подсветкa
    {"nvim-treesitter/nvim-treesitter", run=vim.fn[':TSUpdate']};

    -- Включение и выключения анализа LSP
    "WhoIsSethDaniel/toggle-lsp-diagnostics.nvim";

    -- Автодополнения
    "hrsh7th/nvim-cmp";

    -- Автодополнения интегрировано с LSP
    "hrsh7th/cmp-nvim-lsp";

    -- Дерево файлов NERDTree
    "preservim/nerdtree";

    -- Набор иконок для NERDTree
    "ryanoasis/vim-devicons";

}
