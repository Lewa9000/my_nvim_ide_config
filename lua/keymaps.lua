-- Отключение подсветки при нажатии на запятую + пробел
vim.api.nvim_set_keymap('', ',<space>', ':nohlsearch<CR>',
			{noremap = true, silent = true})

-- Переключатель прозрачного фона
vim.api.nvim_set_keymap('', '<F2>', ':TransparentToggle<CR>',
      {noremap = true, silent = true})

-- Переключатель NERDTree
vim.api.nvim_set_keymap('', '<F3>', ':NERDTreeToggle<CR>',
      {noremap = true, silent = true})

-- Поиск NerdTree
vim.api.nvim_set_keymap('', '<C-F3>', ':NERDTreeFind<CR>',
      {noremap = true, silent = true})

-------------------------------------------------------------------------------
-- LSP Сочетание клавиш
-------------------------------------------------------------------------------
-- Включить и выключить анализ кода с помощью LSP линтера
vim.api.nvim_set_keymap('', '<C-space>', ':ToggleDiag<CR>',
      {noremap = true, silent = true})


